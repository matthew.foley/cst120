# Milestone 1

## Requirements

- Provide a website design consisting of the following:
     - User Interface Sitemap
     - User Interface Wireframes design
     - About Me Page in the Overview Section
     - Recording

- Development Requirements:
     - HyperText Markup Language (HTML) supporting basic page structure
     - HTML with formatting, fonts, colors, etc.
     - HTML images including "float" and "height" properties, controlling text wrapping
     - HTML lists
      
## Example

### Milestone 1

#### Overview Section

- This is Milestone 1 which documents the requirements, development process, screenshots and recording for this week
- Milestone 1 ...

#### References

- This is the Sitemap which shows ...
     - Provide Link to Sitemap
     
![Milestone 1 Sitemap](sitemap.drawio.png)

- This is the Wireframes which shows ...
     - Provide Link to Wireframes

![Milestone 1 Wireframes](wireframes.drawio.png)

- This is the About Me Page ...
     - Provide Image to About Me Page

- This is the Recording ...
     - Provide Link to Recording

#### Conclusion

In **Conclusion** ...


|First name|Last name|
|--|--|
|Matthew|Foley|
|Bobby|Estoy|
